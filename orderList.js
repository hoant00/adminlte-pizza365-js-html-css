    $(document).ready(function () {
        "use strict";
        /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const gBASE_URL_DRINK ="http://42.115.221.44:8080/devcamp-pizza365/drinks";
        const vCREATE_ORDER_BASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
        var gID = 0; // biến lưu giá trị id ngay hàng chi tiết click
        var gOrderId = ""; // biến lưu giá trị order ngay hàng chi tiết click
        var gArrayOrderAjax = []; // biến lưu đối tượng thông tin một order khi gọi API
        var gResultCallCreateOrderAjax = false;
        var gIdDelete = 0;
        var vOrderRowData = [];
        var gTrangThaiOrder = "";
        var gSelectFiter = {
            loaiPizza : [
                {
                    pizzaCode : "Seafood",
                    pizzaName : "Hải sản"
                },
                {
                    pizzaCode : "Hawaii",
                    pizzaName : "Hawaii"
                },
                {
                    pizzaCode : "Bacon",
                    pizzaName : "Thịt hun khói"
                }
            ],
            statusOrder : [
                {
                    statusCode: "open",
                    statusName: "Open"
                },
                {
                    statusCode: "cancel",
                    statusName: "Đã hủy"
                },
                {
                    statusCode: "confirmed",
                    statusName: "Đã xác nhận"
                }
            ]
                
        };
        var vDETAIL_TYPE_COMBO = {
            sizeS : {
                duongKinh: 20,
                suon: 2,
                salad: 200,
                soLuongNuoc: 2,
                donGia: 150000
            },
            sizeM : {
                duongKinh: 25,
                suon: 4,
                salad: 300,
                soLuongNuoc: 3,
                donGia: 200000
            },
            sizeL : {
                duongKinh: 30,
                suon: 8,
                salad: 500,
                soLuongNuoc: 4,
                donGia: 250000
            }
        }
        var gAllDataOrder = {
            kichCo: "",
            duongKinh: 0,
            suon: 0,
            salad: 0,
            soLuongNuoc: 0,
            thanhTien: 0,
            loaiPizza: "",
            idVourcher: 0,
            idLoaiNuocUong: "",
            hoTen: "",
            email: "",
            soDienThoai: 0,
            diaChi: "",
            loiNhan: "",
            trangThai: ""
        }
        var gOrderListDB = {
            reponseOrderList: [],
            filterOrderData: function(paramFilterObj) {
                var vUserFilterResult = [];
                console.log(gOrderListDB.reponseOrderList.length);
                
                // vòng lặp này dùng để chuyển về rỗng những thuộc tính bị null để có thể sử dụng method toLowerCase();
                for(var bI = 0; bI < gOrderListDB.reponseOrderList.length; bI++){
                    if(gOrderListDB.reponseOrderList[bI].trangThai == null && gOrderListDB.reponseOrderList[bI].loaiPizza != null){
                        gOrderListDB.reponseOrderList[bI].trangThai = "";
                    }
                    if(gOrderListDB.reponseOrderList[bI].trangThai != null && gOrderListDB.reponseOrderList[bI].loaiPizza == null){
                        gOrderListDB.reponseOrderList[bI].loaiPizza = "";
                    }
                    if(gOrderListDB.reponseOrderList[bI].trangThai == null && gOrderListDB.reponseOrderList[bI].loaiPizza == null){
                        gOrderListDB.reponseOrderList[bI].loaiPizza = "";
                        gOrderListDB.reponseOrderList[bI].trangThai = "";
                    }
                }
                for(var bI = 0; bI < gOrderListDB.reponseOrderList.length; bI++){
                    
                    //console.log(gOrderListDB.reponseOrderList[bI].loaiPizza.toLowerCase());
                    if(
                        ((paramFilterObj.trangThai == (gOrderListDB.reponseOrderList[bI].trangThai).toLowerCase()) || paramFilterObj.trangThai === "") 
                    && ((paramFilterObj.loaiPizza.toLowerCase() == (gOrderListDB.reponseOrderList[bI].loaiPizza).toLowerCase()) || paramFilterObj.loaiPizza === ""))
                    {
                        vUserFilterResult.push(gOrderListDB.reponseOrderList[bI]);
                    }
                }
            return vUserFilterResult;
            }
        }
        var Toast = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            timer: 3000
        });
        var gFORM_MODE_NORMAL = "Normal";
        var gFORM_MODE_INSERT = "Insert";
        var gFORM_MODE_UPDATE = "Update";
        var gFORM_MODE_DELETE = "Delete";
        // biến toàn cục cho trạng thái của form: mặc định ban đầu là trạng thái Normal
        var gFormMode = gFORM_MODE_NORMAL;
        const gNameColOrder = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
        const gORDER_ID_COL = 0;
        const gSIZE_COL = 1;
        const gPIZZA_TYPE_COL = 2;
        const gDRINK_COL = 3;
        const gTHANH_TIEN_COL = 4;
        const gFULLNAME_COL = 5;
        const gSO_DIEN_THOAI_COL = 6;
        const gTRANG_THAI_COL = 7;
        const gACTION_COL = 8;
        // Khai báo DataTable & mapping collumns
        var gOrderListTable = $("#table-order").DataTable({
            paging: false,
            searching: false,
            columns: [
            { data: gNameColOrder[gORDER_ID_COL] },
            { data: gNameColOrder[gSIZE_COL] },
            { data: gNameColOrder[gPIZZA_TYPE_COL] },
            { data: gNameColOrder[gDRINK_COL] },
            { data: gNameColOrder[gTHANH_TIEN_COL] },
            { data: gNameColOrder[gFULLNAME_COL] },
            { data: gNameColOrder[gSO_DIEN_THOAI_COL] },
            { data: gNameColOrder[gTRANG_THAI_COL] },
            { data: gNameColOrder[gACTION_COL] }
            ],
            columnDefs: [
            { // định nghĩa lại cột action
                targets: gACTION_COL,
                defaultContent: `
                <i class="far fa-edit text-primary" type="button" aria-hidden="true"></i>
                <i class="fas fa-trash-alt text-danger" type="button" aria-hidden="true"></i>
                `
            }
            ]
        });
        /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
        onPageLoading();
        // lọc
        $("#filterDataBtn").on("click", function() {
            console.log("Nút lọc được click!");
            onBtnFilterClick();
        });
        // tạo đơn hàng
        $("#btn-create-order").on("click", function() {
            console.log("Nút tạo đơn hàng được click!");
            onBtnCreateOrderClick();
        });
        $(document).on("change", "#select-combo", function() {
            onSelectComboChange();
          });
        // nút save data trên modal được click
        $(document).on("click", "#btn-save-order", function() {
            console.log("Nút Save data được click!");
            onBtnSaveDataClick();
        });
        // 3 - U: gán sự kiện Update - Sửa 1 student
        $(document).on("click", ".fa-edit", function() {
            console.log("Icon Edit được click!");
            onIconEditOrderClick(this);
        });
        $(document).on("click", "#edit-order-btn", function() {
            console.log("Edit trên modal được click!");
            onBtnEditOrderModalClicked();
        });
        // nút cancel trên modal được click
        $(document).on("click", ".fa-trash-alt", function() {
            console.log("Icon delete được click được click!");
            onIconDeleteClick(this);
        });
        $(document).on("click", "#btn-confirm-delete-order", function() {
            console.log("Confirm delete trên modal được click!");
            onBtnDeleteOrderClicked();
        });
        // thêm sự kiện đổi trạng thái khi ẩn các modal----------------
        $("#add-edit-order-modal").on("hidden.bs.modal", function() {
            gFormMode = gFORM_MODE_NORMAL;
            $("#div-form-mod").html(gFormMode);
            resetModalForm();
        });
        $("#result-create-order").on("hidden.bs.modal", function() {
            gFormMode = gFORM_MODE_NORMAL;
            $("#div-form-mod").html(gFormMode);
        });
        $("#edit-order-modal").on("hidden.bs.modal", function() {
            gFormMode = gFORM_MODE_NORMAL;
            $("#div-form-mod").html(gFormMode);
        });
        $("#delete-confirm-modal").on("hidden.bs.modal", function() {
            gFormMode = gFORM_MODE_NORMAL;
            $("#div-form-mod").html(gFormMode);
        });
        /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        // Hàm thực hiện khi load trang
        function onPageLoading() {
            "use strict";
            //b1: thu thập dữ liệu không cần
            //b2: valid dữ liệu không cần
            //b3: gọi API
            callApiToGetAllOrdersList();
            //b4: đổ dữ liệu vào bảng;
            loadDataToOrderTable(gOrderListDB.reponseOrderList);
            loadDataToSelectPizzaType(gSelectFiter.loaiPizza);
            loadDataToSelectStatus(gSelectFiter.statusOrder);
            $("#div-form-mod").html(gFormMode);
        }
        // nút tạo đơn hàng click
        function onBtnCreateOrderClick(){
            "use strict";
            // b1: thu thập data (bỏ qua)
            //b2: valid (bỏ qua)
            // chuyển đổi trạng thái form về insert
            gFormMode = gFORM_MODE_INSERT;
            $("#div-form-mod").html(gFormMode);
            // hiển thị modal trắng lên
            $("#add-edit-order-modal").modal("show");
            // b3: gọi API , b4: sau khi gọi sẽ đồng thời show to seclect drink
            getDataFormAjaxToSelectDrink();
            //$("#input-username").prop("readonly",false); -------------------- sử dụng cho edit order
            // đổ dữ liệu mặc định vào các trường vì size S đang được chọn mặc định
            $("#input-pizza-size").val(vDETAIL_TYPE_COMBO.sizeS.duongKinh);
            $("#input-suon-nuong").val(vDETAIL_TYPE_COMBO.sizeS.suon);
            $("#input-salad").val(vDETAIL_TYPE_COMBO.sizeS.salad);
            $("#input-drink-amount").val(vDETAIL_TYPE_COMBO.sizeS.soLuongNuoc);
            $("#input-price").val(vDETAIL_TYPE_COMBO.sizeS.donGia);
            gAllDataOrder.kichCo = "S";
            gAllDataOrder.duongKinh = vDETAIL_TYPE_COMBO.sizeS.duongKinh;
            gAllDataOrder.suon = vDETAIL_TYPE_COMBO.sizeS.suon;
            gAllDataOrder.salad = vDETAIL_TYPE_COMBO.sizeS.salad;
            gAllDataOrder.soLuongNuoc = vDETAIL_TYPE_COMBO.sizeS.soLuongNuoc;
            gAllDataOrder.thanhTien = vDETAIL_TYPE_COMBO.sizeS.donGia;
        }
        // xử lý khi nút save data được click
        function onBtnSaveDataClick(){
            "use strict";
            if(gFormMode = gFORM_MODE_INSERT){
                // b1: thu thập dữ liệu
                getDataFromForm();
                //b2: valid dữ liệu
                var vCheck = validOrderData();
                if(vCheck){
                    // b3: gọi API
                    callAPICreateOrder();
                    if(gResultCallCreateOrderAjax){
                        callApiEditStatusById(vOrderRowData.id, gAllDataOrder.trangThai);
                        // gAllDataOrder.trangThai = gTrangThaiOrder;
                        // console.log(gAllDataOrder.trangThai);
                        showInfoOrderCreated(gTrangThaiOrder);
                    }
                    else{
                        Toast.fire({
                            icon: 'error',
                            title: 'Tạo đơn hàng thất bại, có thể do đường truyền, hãy thử lại sau!'
                        });
                    }
                $("#add-edit-order-modal").modal("hide");
                resetModalForm();
                }
                console.log(gAllDataOrder);
            }
            gResultCallCreateOrderAjax = false;
        }
        function onBtnEditOrderModalClicked(){
            "use strict";
            var vSelectStatus = $("#input-status-order-edit option:selected").val();
                callApiEditStatusById(gID, vSelectStatus);
                Toast.fire({
                    icon: 'success',
                    title: 'Cập nhật dữ liệu thành công'
                })
                $("#edit-order-modal").modal("hide");
                resetModalForm();
                location.reload();
        }
        // hàm xử lý khi nút lọc được click
        function onBtnFilterClick(){
            "use strict";
            // Khai báo đối tượng chứa dữ liệu lọc trên form
            var vOrderFilterDataObj = {
                trangThai: "",
                loaiPizza: ""
            }
            //b1: thu thập data
            getFilterData(vOrderFilterDataObj);
            console.log(vOrderFilterDataObj);
            //b2: valid data (khong can)
            // b3: Thực hiện nghiệp vụ lọc
            if(vOrderFilterDataObj.trangThai != "" || vOrderFilterDataObj.loaiPizza != ""){
                var vOrderFilterResult = gOrderListDB.filterOrderData(vOrderFilterDataObj);
                console.log(vOrderFilterResult);
                // b4: load dữ liệu lên datatable
                loadDataToOrderTable(vOrderFilterResult);
            }
            else{
                // b4: load dữ liệu lên datatable
                loadDataToOrderTable(gOrderListDB.reponseOrderList);
            }
        }
        
        // hàm call API và đổ vào select drink
        function getDataFormAjaxToSelectDrink(){
            "use strict";
            // b1: thu thập dữ liệu (bở qua)
            // b2: valid (bở qua)
            // b3: call API
                // gọi api load dữ liệu users
                $.ajax({
                url: gBASE_URL_DRINK,
                type: 'GET',
                dataType: 'json',
                async: false,
                success: function (objectDrink) {
                    console.log(objectDrink);
                    // b4: hiển thị dữ liệu đến select drink
                    showToSelectDrink(objectDrink);
                }
                });
        }
        // hàm xử lý khi edit được click
        function onIconEditOrderClick(paramIconEdit){
            "use strict";
            // chuyển đổi trạng thái form về update
            gFormMode = gFORM_MODE_UPDATE;
            $("#div-form-mod").html(gFormMode);
            // lưu thông tin id, student Id và subject Id đang được edit vào biến toàn cục
            getIdDataFromButton(paramIconEdit);
            // load dữ liệu vào các trường dữ liệu trong modal
            showOrderDataToModal(paramIconEdit);
            //hiển thị modal lên
            $("#edit-order-modal").modal("show");
        }
        // nut icon clcik
        function onIconDeleteClick(paramicondelete){
            "use strict";
            gFormMode = gFORM_MODE_DELETE;  
            $("#div-form-mod").html(gFormMode);
            $("#delete-confirm-modal").modal("show");
            //b1: thu thập dữ liệu 
            var vTableRow = $(paramicondelete).parents("tr");
            var vOrderRowData = gOrderListTable.row(vTableRow).data();
            console.log(vOrderRowData);
            gIdDelete = vOrderRowData.id;
        }
        // hàm xử lý khi nút confirm order được click
        function onBtnDeleteOrderClicked(){
            "use strict";
            // b3: call API
            callAPIToDeleteOrder();
            $("#delete-confirm-modal").modal("hide");
            location.reload();
        }
        /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        // hàm gọi api để lấy all orders (đã sử dụng)----------------------------------------------
        function callApiToGetAllOrdersList() {
            "use strict";
            // gọi api load dữ liệu users
            $.ajax({
            url: 'http://42.115.221.44:8080/devcamp-pizza365/orders',
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (paramObjectOrder) {
                gOrderListDB.reponseOrderList = paramObjectOrder;
                console.log(gOrderListDB.reponseOrderList);
            }
            });
        }
        // load dữ liệu lên table (đã sử dụng)---------------------------------------------
        function loadDataToOrderTable(paramOrder) {
            "use strict";
            //gSTT = 1;
            gOrderListTable.clear();
            gOrderListTable.rows.add(paramOrder);
            gOrderListTable.draw();
        }
        // hàm thu thập dữ liệu lọc trên form ------------------------------------------ đã sữ dụng
        function getFilterData(paramFilterObj) { 
            "use strict";
            paramFilterObj.trangThai = $("#select-status option:selected").val();
            paramFilterObj.loaiPizza = $("#select-pizza-type option:selected").val();
        }
        
        // hàm load dữ liệu đến select type pizza (đã sử dụng)------------------------------------------
        function loadDataToSelectPizzaType(parmamLoaiPizza) {
            "use strict";
            var vSelectPizza = $("#select-pizza-type");
            //lấy dữ liệu duỵet cho vào các option tiếp theo
            for (let bI = 0; bI < parmamLoaiPizza.length; bI++) {
                var bStudentOption = $(
                "<option/>",
                {
                    text: parmamLoaiPizza[bI].pizzaName,
                    value: parmamLoaiPizza[bI].pizzaCode
                }
                ).appendTo(vSelectPizza);
            }
        }
        // hàm load dữ liệu đến select type pizza (đã sử dụng)--------------------------------------------
        function loadDataToSelectStatus(parmamStatus) {
            "use strict";
            var vSelectStatus = $("#select-status");
            //lấy dữ liệu duỵet cho vào các option tiếp theo
            for (let bI = 0; bI < parmamStatus.length; bI++) {
                var bStudentOption = $(
                "<option/>",
                {
                    text: parmamStatus[bI].statusName,
                    value: parmamStatus[bI].statusCode
                }
                ).appendTo(vSelectStatus);
            }
        }
        // hàm xử lý khi thay đổi select combo kích cỡ trên modal -------------------------------------------------đã sử dụng
        function onSelectComboChange(){
            "use strict";
            var vSelected = $("#select-combo").val();
            console.log(vSelected);
            if(vSelected == "s"){
                $("#input-pizza-size").val(vDETAIL_TYPE_COMBO.sizeS.duongKinh);
                $("#input-suon-nuong").val(vDETAIL_TYPE_COMBO.sizeS.suon);
                $("#input-salad").val(vDETAIL_TYPE_COMBO.sizeS.salad);
                $("#input-drink-amount").val(vDETAIL_TYPE_COMBO.sizeS.soLuongNuoc);
                $("#input-price").val(vDETAIL_TYPE_COMBO.sizeS.donGia);
                gAllDataOrder.kichCo = "S";
                gAllDataOrder.duongKinh = vDETAIL_TYPE_COMBO.sizeS.duongKinh;
                gAllDataOrder.suon = vDETAIL_TYPE_COMBO.sizeS.suon;
                gAllDataOrder.salad = vDETAIL_TYPE_COMBO.sizeS.salad;
                gAllDataOrder.soLuongNuoc = vDETAIL_TYPE_COMBO.sizeS.soLuongNuoc;
                gAllDataOrder.thanhTien = vDETAIL_TYPE_COMBO.sizeS.donGia;
            }
            if(vSelected == "m"){
                $("#input-pizza-size").val(vDETAIL_TYPE_COMBO.sizeM.duongKinh);
                $("#input-suon-nuong").val(vDETAIL_TYPE_COMBO.sizeM.suon);
                $("#input-salad").val(vDETAIL_TYPE_COMBO.sizeM.salad);
                $("#input-drink-amount").val(vDETAIL_TYPE_COMBO.sizeM.soLuongNuoc);
                $("#input-price").val(vDETAIL_TYPE_COMBO.sizeM.donGia);
                gAllDataOrder.kichCo = "M";
                gAllDataOrder.duongKinh = vDETAIL_TYPE_COMBO.sizeM.duongKinh;
                gAllDataOrder.suon = vDETAIL_TYPE_COMBO.sizeM.suon;
                gAllDataOrder.salad = vDETAIL_TYPE_COMBO.sizeM.salad;
                gAllDataOrder.soLuongNuoc = vDETAIL_TYPE_COMBO.sizeM.soLuongNuoc;
                gAllDataOrder.thanhTien = vDETAIL_TYPE_COMBO.sizeM.donGia;
            }
            if(vSelected == "l"){
                $("#input-pizza-size").val(vDETAIL_TYPE_COMBO.sizeL.duongKinh);
                $("#input-suon-nuong").val(vDETAIL_TYPE_COMBO.sizeL.suon);
                $("#input-salad").val(vDETAIL_TYPE_COMBO.sizeL.salad);
                $("#input-drink-amount").val(vDETAIL_TYPE_COMBO.sizeL.soLuongNuoc);
                $("#input-price").val(vDETAIL_TYPE_COMBO.sizeL.donGia);
                gAllDataOrder.kichCo = "L";
                gAllDataOrder.duongKinh = vDETAIL_TYPE_COMBO.sizeL.duongKinh;
                gAllDataOrder.suon = vDETAIL_TYPE_COMBO.sizeL.suon;
                gAllDataOrder.salad = vDETAIL_TYPE_COMBO.sizeL.salad;
                gAllDataOrder.soLuongNuoc = vDETAIL_TYPE_COMBO.sizeL.soLuongNuoc;
                gAllDataOrder.thanhTien = vDETAIL_TYPE_COMBO.sizeL.donGia;
            }
        }
        // hàm đổ dữ liệu đến select drink ---------------------------------------------------------đã sử dụng
        function showToSelectDrink(paramObjectDrink){
            "use strict";
            for(var bI = 0; bI < paramObjectDrink.length; bI ++){
                var vOption = $("<option>").val(paramObjectDrink[bI].maNuocUong).text(paramObjectDrink[bI].tenNuocUong);
                $("#input-type-drink-modal").append(vOption);
            }
        }
        // hàm thu thập dữ liệu khi nút save data được click
        function getDataFromForm(){
            "use strict";
            gAllDataOrder.loaiPizza = $("#select-pizza-type-modal option:selected").val();
            gAllDataOrder.idVourcher = $("#input-idvoucher").val();
            gAllDataOrder.idLoaiNuocUong = $("#input-type-drink-modal option:selected").val();
            gAllDataOrder.hoTen = $("#input-hoten").val();
            gAllDataOrder.email = $("#input-email").val();
            gAllDataOrder.soDienThoai = $("#input-sodienthoai").val();
            gAllDataOrder.diaChi = $("#input-dia-chi").val();
            gAllDataOrder.loiNhan = $("#input-loi-nhan").val();
            gAllDataOrder.trangThai = $("#select-status-order-modal option:selected").val();
            console.log(gAllDataOrder);
        }
        // hàm reset modal
        function resetModalForm(){
            "use strict";
            // đưa thẻ p về normal
            gFormMode = gFORM_MODE_NORMAL;
            $("#div-form-mod").html(gFormMode);
            // đưa các trường modal về mặc định
            $("#select-combo").val("s");
            $("#select-pizza-type-modal option:selected").val("Seafood");
            $("#input-idvoucher").val("");
            $("#input-type-drink-modal").empty();
            $("#input-hoten").val("");
            $("#input-email").val("");
            $("#input-sodienthoai").val("");
            $("#input-dia-chi").val("");
            $("#input-loi-nhan").val("");
            $("#select-status-order-modal option:selected").val("open");
        }
        // hàm valid dữ liệu
        function validOrderData(){
            "use strict";
            var vMailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if((gAllDataOrder.idVourcher != "") && !(Number.isInteger(parseInt(gAllDataOrder.idVourcher)))){
                Toast.fire({
                    icon: 'error',
                    title: 'Voucher có thể không nhập, nhưng nếu nhập phải là số nguyên!'
                });
                return false;
            }
            if(gAllDataOrder.hoTen == ""){
                Toast.fire({
                    icon: 'error',
                    title: 'Họ tên phải nhập!'
                });
                return false;
            }
            if((gAllDataOrder.email != "") && vMailFormat.test(gAllDataOrder.email) == false){
                Toast.fire({
                    icon: 'error',
                    title: 'Sai định dạng! email có thể không nhập, nếu nhập phải đúng định dạng!'
                });
                return false;
            }
            if(gAllDataOrder.soDienThoai == ""){
                Toast.fire({
                    icon: 'error',
                    title: 'Số điện thoại phải nhập!'
                });
                return false;
            }
            if(gAllDataOrder.diaChi == ""){
                Toast.fire({
                    icon: 'error',
                    title: 'Địa chỉ phải nhập!'
                });
                return false;
            }
            return true;
        }
        
        // hàm gọi API tạo order
        function callAPICreateOrder(){
            "use strict";
            vOrderRowData = [];
            $.ajax({
            url: vCREATE_ORDER_BASE_URL,
            type: 'POST',
            async: false,
            contentType: 'application/json;charset=UTF-8', // added data type
            data : JSON.stringify(gAllDataOrder),
            success: function (newOrderObj) {
                console.log(Object.entries(newOrderObj));
                gResultCallCreateOrderAjax = true;
                vOrderRowData = newOrderObj;
                console.log(vOrderRowData);
            },
            error: function (ajaxContext) {
                gResultCallCreateOrderAjax = false;
            }
            });
        }
        // hàm hiển thị lên modal kết quả tạo order
        function showInfoOrderCreated(paramTrangThaiOrder){
            "use strict";
            var vValueVoucher = "";

            $("#input-status-order-result").empty();
            if((parseInt(vOrderRowData.thanhTien) - vOrderRowData.giamGia) <= 0 || vOrderRowData.giamGia < 0){
                vValueVoucher = "Không có";
                vOrderRowData.giamGia = 0;
            }
            else{
                vValueVoucher = vOrderRowData.idVourcher;
            }
            $("#add-edit-order-modal").modal("hide");
            $("#result-create-order").modal("show");
            $("#select-combo-result").val(vOrderRowData.kichCo);
            $("#input-pizza-size-result").val(vOrderRowData.duongKinh);
            $("#input-suon-nuong-result").val(vOrderRowData.suon);
            $("#input-salad-result").val(vOrderRowData.salad);
            $("#input-drink-amount-result").val(vOrderRowData.soLuongNuoc);
            $("#input-price-result").val(vOrderRowData.thanhTien);
            $("#input-pizza-type-result").val(vOrderRowData.loaiPizza);
            $("#input-idvoucher-result").val(vValueVoucher);
            $("#input-type-drink-result").val(vOrderRowData.idLoaiNuocUong);
            $("#input-hoten-result").val(vOrderRowData.hoTen);
            $("#input-email-result").val(vOrderRowData.email);
            $("#input-sodienthoai-result").val(vOrderRowData.soDienThoai);
            $("#input-dia-chi-result").val(vOrderRowData.diaChi);
            $("#input-loi-nhan-result").val(vOrderRowData.loiNhan);
            var addOptionStatus = $("#input-status-order-result");
            var bSubjectOption = $(
                "<option/>",
                {
                  text: paramTrangThaiOrder,
                  value: ""
                }
              ).appendTo(addOptionStatus);
            $("#input-discount-result").val(vOrderRowData.giamGia);
            var vPhaiThanhToanModal = vOrderRowData.thanhTien - vOrderRowData.giamGia
            $("#input-end-money-result").val(vPhaiThanhToanModal);
            $("#input-order-id-result").val(vOrderRowData.orderId);
            $("#input-ID-result").val(vOrderRowData.id);
            $("#input-ngay-tao-result").val(vOrderRowData.ngayTao);
            $("#input-ngay-cap-nhat-result").val(vOrderRowData.ngayCapNhat);
            vOrderRowData = [];
        }
        // hàm gọi update voucher
        // hàm gọi Api để đặt lại trạng thái
        function callApiEditStatusById(paramID, paramTrangThai){
            "use strict";
            var vOjectStatus = {
                trangThai : paramTrangThai
            }
            // phải implement ajax put call tại đây
            $.ajax({
              url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + paramID,
              type: 'PUT',
              async: false,
              contentType: 'application/json', // added data type
              data : JSON.stringify(vOjectStatus),
              success: function (res) {
                console.log(Object.entries(res));
                gTrangThaiOrder = res.trangThai;
              },
              error: function (ajaxContext) {
                alert(ajaxContext.responseText)
              }
            });
          }
        // hàm tìm id grade từ student code and subject code
        function getIdDataFromButton(paramIcon){
            "use strict";
            var vTableRow = $(paramIcon).parents("tr");
            var vOrderRowData = gOrderListTable.row(vTableRow).data();
            console.log(vOrderRowData);
            gID = vOrderRowData.id;
        }
        // hàm hiển thị order to modal
        function showOrderDataToModal(paramIconEdit){
            "use strict";
            var vTableRow = $(paramIconEdit).parents("tr");
            var vOrderRowData = gOrderListTable.row(vTableRow).data();
            $("#select-combo-edit").val(vOrderRowData.kichCo);
            $("#select-combo-edit").prop("readonly",true);
            $("#input-pizza-size-edit").val(vOrderRowData.duongKinh);
            $("#input-pizza-size-edit").prop("readonly",true);
            $("#input-suon-nuong-edit").val(vOrderRowData.suon);
            $("#input-suon-nuong-edit").prop("readonly",true);
            $("#input-salad-edit").val(vOrderRowData.salad);
            $("#input-salad-edit").prop("readonly",true);
            $("#input-drink-amount-edit").val(vOrderRowData.soLuongNuoc);
            $("#input-drink-amount-edit").prop("readonly",true);
            $("#input-price-edit").val(vOrderRowData.thanhTien);
            $("#input-price-edit").prop("readonly",true);
            $("#input-pizza-type-edit").val(vOrderRowData.loaiPizza);
            $("#input-pizza-type-edit").prop("readonly",true);
            $("#input-idvoucher-edit").val(vOrderRowData.idVourcher);
            $("#input-idvoucher-edit").prop("readonly",true);
            $("#input-type-drink-edit").val(vOrderRowData.idLoaiNuocUong);
            $("#input-type-drink-edit").prop("readonly",true);
            $("#input-hoten-edit").val(vOrderRowData.hoTen);
            $("#input-hoten-edit").prop("readonly",true);
            $("#input-email-edit").val(vOrderRowData.email);
            $("#input-email-edit").prop("readonly",true);
            $("#input-sodienthoai-edit").val(vOrderRowData.soDienThoai);
            $("#input-sodienthoai-edit").prop("readonly",true);
            $("#input-dia-chi-edit").val(vOrderRowData.diaChi);
            $("#input-dia-chi-edit").prop("readonly",true);
            $("#input-loi-nhan-edit").val(vOrderRowData.loiNhan);
            $("#input-loi-nhan-edit").prop("readonly",true);
            $("#input-status-order-edit").empty();
            var vSelectStatus = $("#input-status-order-edit");
            for (let bI = 0; bI < gSelectFiter.statusOrder.length; bI++) {
                var bSubjectOption = $(
                  "<option/>",
                  {
                    text: gSelectFiter.statusOrder[bI].statusName,
                    value: gSelectFiter.statusOrder[bI].statusCode
                  }
                ).appendTo(vSelectStatus);
              }
            $("#input-status-order-edit").val(vOrderRowData.trangThai);
            $("#input-discount-edit").val(vOrderRowData.giamGia);
            $("#input-discount-edit").prop("readonly",true);
            $("#input-end-money-edit").val(vOrderRowData.thanhTien - vOrderRowData.giamGia);
            $("#input-end-money-edit").prop("readonly",true);
            $("#input-order-id-edit").val(vOrderRowData.orderId);
            $("#input-order-id-edit").prop("readonly",true);
            $("#input-ID-edit").val(vOrderRowData.id);
            $("#input-ID-edit").prop("readonly",true);
            $("#input-ngay-tao-edit").val(vOrderRowData.ngayTao);
            $("#input-ngay-tao-edit").prop("readonly",true);
            $("#input-ngay-cap-nhat-edit").val(vOrderRowData.ngayCapNhat);
            $("#input-ngay-cap-nhat-edit").prop("readonly",true);
        }
        // hàm call API xóa order
        function callAPIToDeleteOrder(){
            "use strict";
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gIdDelete,
                type: 'DELETE',
                async: false,
                contentType: 'application/json', // added data type
                success: function (res) {
                    Toast.fire({
                        icon: 'success',
                        title: 'Xóa đơn hàng thành công'
                    })
                  
                },
                error: function (ajaxContext) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Lỗi xóa đơn hàng, có thể do mạng!'
                    })
                }
              });
        }
    });